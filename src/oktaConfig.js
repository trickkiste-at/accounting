require('dotenv').config()
console.log(process.env)

const { VUE_APP_CLIENT_ID, VUE_APP_ISSUER, VUE_APP_OKTA_TESTING_DISABLEHTTPSCHECK, VUE_APP_BASE_URL } = process.env
console.log("CLIENT_ID: " + VUE_APP_CLIENT_ID)
console.log("ISSUER: " + VUE_APP_ISSUER)
console.log("BASE_URL: " + VUE_APP_BASE_URL)

export default {
  oidc: {
    clientId: VUE_APP_CLIENT_ID,
    issuer: VUE_APP_ISSUER,
    redirectUri: VUE_APP_BASE_URL + '/login/callback',
    scopes: ['openid', 'profile', 'email', ''],
    pkce: true,
    testing: {
      disableHttpsCheck: VUE_APP_OKTA_TESTING_DISABLEHTTPSCHECK
    }
  },
  resourceServer: {
    messagesUrl: 'https://test.trickkiste.at/api/messages'
  }
}
