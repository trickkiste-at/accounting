import Vue from 'vue'
import VueRouter from 'vue-router'
import 'semantic-ui-css/semantic.min.css'

import { OktaAuth } from '@okta/okta-auth-js'
import OktaVue from '@okta/okta-vue'

import HomeComponent from '../views/Home.vue'
import LoginComponent from '@/components/Login'
import ProfileComponent from '@/components/Profile'
import UploadComponent from '@/components/Upload'

import oktaConfig from '@/oktaConfig'
const oktaAuth = new OktaAuth(oktaConfig.oidc)

Vue.use(VueRouter)
Vue.use(OktaVue, {
  oktaAuth,
  onAuthRequired: () => {
    router.push('/login')
  }
})

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeComponent
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginComponent
  },
  {
    path: '/profile',
    name: 'Profile',
    component: ProfileComponent,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/upload',
    name: 'Upload',
    component: UploadComponent,
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
