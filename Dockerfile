FROM node:lts-alpine3.10

RUN apk update && apk upgrade && \
  apk add --no-cache bash git openssh

RUN mkdir /app
WORKDIR /app

RUN yarn global add @vue/cli

COPY package.json .

RUN yarn install

COPY . .

#RUN npm run build
#ENV NODE_ENV production
#RUN npm install --production

EXPOSE 8080

CMD ["yarn", "serve"]
